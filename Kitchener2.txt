Benefits to the Community:
Expanded rail service will provide the Guelph-Wellington Region with more direct connections to the larger GO Transit network, helping to reduce congestion in the GTHA while connecting people to education and job opportunities.

Better connectivity between Guelph-Wellington and the rest of the Greater Toronto and Hamilton Area, supports economic development and urban development plans in the region.